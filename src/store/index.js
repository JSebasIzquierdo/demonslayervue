import { createStore } from "vuex";
import {login} from "./Modulos/auth";

const store = createStore({
  modules: {
    login,
  },
});

export default store;
