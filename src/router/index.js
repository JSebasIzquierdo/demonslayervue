import { createRouter, createWebHistory } from "vue-router";
import Login from "@/components/Login/index.vue";
import SignUp from "../components/SignUp/Index.vue";
import BackOffice from "../components/BackOffice/index.vue";
import CreateCategorie from '../components/BackOffice/CreateCategorie/index.vue';


const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/signUp",
    name: "signUp",
    component: SignUp,
  },
  {
    path: "/backOffice",
    name: "backOffice",
    component: BackOffice,
    
  },

  {
    path: "/backOffice/categories",
    name: "backOfficeCategories",
    component: CreateCategorie,

  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});



export default router;
